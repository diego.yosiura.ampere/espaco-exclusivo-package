from ee_ampere_consultoria.produtos.flux import FluxUpload
from ee_ampere_consultoria.produtos.flux import BodyFluxUpload
from ee_ampere_consultoria.produtos.flux import DimensoesAgrupamento
import datetime
import hashlib

# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
d1 = hoje + datetime.timedelta(days=1)
df = datetime.datetime(2023, 5, 19)
zip_upload_file = "caminho/do/seu/arquivo/.zip"

options = BodyFluxUpload()
# opções para arquivos de saída
options.set_option_produzir_mapas(True)
options.set_option_produzir_arquivo_pmed_xlsx(False)
options.set_option_produzir_arquivos_dessem(False)
options.set_option_produzir_ena_diaria(True)
options.set_option_tabelar_prevs_produzidos(False)
options.set_option_produzir_prevs_mensais(False)
options.set_option_produzir_vazpast(True)

# opções de uso do modelo previvaz
# options.set_option_previvaz_produzir_todas_revisoes_intermediarias(True)
# options.set_option_previvaz_produzir_somente_rv0_intermediarias(True)
options.set_option_previvaz_produzir_somente_revisoes_rvf(True)

# # opções de produção de vazões probabilísticas
# options.set_option_produzir_vmed(True)
# options.set_option_produzir_vpercentil(True)
# options.set_option_cenarios_vpercentil([17, 25, 34, 50, 67, 75, 84])
#
# # opções de produção de vazões por agrupamento
# options.set_option_produzir_cenarios_por_agrupamento(True)
# options.set_option_numero_agrupamentos(3)
# options.set_option_dimensoes_para_agrupamento(DimensoesAgrupamento.SE_NE_vs_S_N.value)

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"

# EXECUÇÃO --------------------------------------------------------------
flux = FluxUpload(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)
rq = flux.upload_file("NOME-ESTUDO-UPLOAD", d1, df, zip_upload_file, options)


# VERIFICAÇÃO DO REQUEST ------------------------------------------------
msg = """
Estudo {NOME} enviado para simulação.
Numeração: {ID}.
Código de identificação única UUID: {UUID}
"""
msg = msg.format(NOME=rq['ds_nome_estudo'], ID=rq['id'], UUID=rq['uuid'])
print(msg)


