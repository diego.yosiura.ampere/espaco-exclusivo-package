# FLUX Personalizado
>>>
Metodos e procedimentos para acesso à ferramenta FLUX Personalizado.
>>>

## Importação
```python
# Importa o objeto responsável pelas requisições e autenticação.
from ee_ampere_consultoria.produtos.flux import FluxPersonalizado

# Importa o objeto responsável pela criação do estudo. 
from ee_ampere_consultoria.produtos.flux import BodyFluxPersonalizado

#classe auxiliar de modelos.
from ee_ampere_consultoria.produtos.meteorologia import Modelos
```

## Método get_list() -> list
```python
fl = FluxPersonalizado('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                       proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
print(fl.get_list())
```
`output`
```json
[
  {"id": "id", "value": 123},
  {"id": "ds_nome_estudo", "value": "ESTUDO-ESTE-API"},
  {"id": "id_empresa", "value": 91},
  {"id": "id_user", "value": 871},
  {"id": "dt_inicio", "value": 1627129830},
  {"id": "dt_fim", "value": 1633177830},
  {"id": "dt_last_execution", "value": 1627065660},
  {"id": "dt_last_response", "value": 1627066848},
  {"id": "vl_execution_counter", "value": 1},
  {"id": "ready", "value": true},
  {"id": "ck_internal_error", "value": false},
  {
    "id": "cenarios",
    "value": [
      {
        "id": 5374,
        "id_estudo": 123,
        "ds_nome": "ESTUDO-ESTE-API",
        "blocos": [
          {"id": 9175, "id_cenario": 5374, "ds_modelo": "ecmwf46", "dt_data_prev": 1626957030, "ck_rmv": false, "dt_inicio": 1627129830, "dt_fim": 1630845030},
          {"id": 9176, "id_cenario": 5374, "ds_modelo": "climatologia", "dt_data_prev": 1627043430, "ck_rmv": false, "dt_inicio": 1630845030, "dt_fim": 1633177830}
        ]
      }
    ]
  }
]
```

## Método get_download_link(id_estudo: int) -> str
>>>
Retorna o link para download dos resultados do estudo enviado.
>>>
```python
fl = FluxPersonalizado('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.get_download_link(2895))
```


## Método get_preview(id_estudo: int) -> str
>>>
Retorna a composicao dos mapas para o estudo `id_estudo`
>>>
```python
fl = FluxPersonalizado('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.get_preview(2898))
```


## Método create_request(self, estudo: BodyFluxPersonalizado) -> dict
>>>
Criando o estudo. 
>>>
```python

body = BodyFluxPersonalizado()
body.set_nome_estudo("TESTE-ESTUDO-API-159_55")
body.set_periodo_analise(datetime(2021, 7, 24), datetime(2022, 1, 19))

body.add_bloco(Modelos.ETA, False, datetime(2021, 7, 23), datetime(2021, 7, 24), datetime(2021, 8, 2))
body.add_bloco(Modelos.INFORMES, False, datetime(2021, 7, 23), datetime(2021, 8, 3), datetime(2022, 1, 19))
print(body.get_json())
```
`output`
```json
{
  "ds_nome_estudo": "TESTE-ESTUDO-API-159_55",
  "ds_nome_cenario": "TESTE-ESTUDO-API-159_55",
  "dt_inicio": 1627095600,
  "dt_fim": 1642561200,
  "cenarios": [
    {
      "ds_nome": "TESTE-ESTUDO-API-159_55",
      "blocos": [
        {
          "ds_modelo": "eta",
          "dt_data_prev": 1627009200,
          "ck_rmv": false,
          "dt_inicio": 1627095600,
          "dt_fim": 1627873200
        },
        {
          "ds_modelo": "informes",
          "dt_data_prev": 1627009200,
          "ck_rmv": false,
          "dt_inicio": 1627959600,
          "dt_fim": 1642561200
        }
      ]
    }
  ]
}
```
>>>
Executando a requisição
>>>

```python
fl = FluxPersonalizado('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
rq = fl.create_request(body)
print(fl.execute(rq))
```
`output`
```json
{
  "id": 2900,
  "id_empresa": 1,
  "id_user": 1,
  "ds_nome_estudo": "TESTE-ESTUDO-API-159_55",
  "dt_inicio": 1627095600,
  "dt_fim": 1642561200,
  "dt_last_execution": null,
  "dt_last_response": null,
  "vl_execution_counter": -1,
  "ready": false,
  "uuid": "6253faba-ec01-11eb-bfbe-02001700dcdd",
  "ck_internal_error": false,
  "ck_is_file": false,
  "ck_is_nt": false,
  "cenarios": [
    {
      "id": 5379,
      "id_estudo": 2900,
      "ds_nome": "TESTE-ESTUDO-API-159_55",
      "uuid": "6254551e-ec01-11eb-bfbe-02001700dcdd",
      "blocos": [
        {
          "id": 9181,
          "id_cenario": 5379,
          "ds_modelo": "eta",
          "dt_data_prev": 1627009200,
          "ck_rmv": false,
          "dt_inicio": 1627095600,
          "dt_fim": 1627873200,
          "preview": null,
          "uuid": "62549d62-ec01-11eb-bfbe-02001700dcdd"
        },
        {
          "id": 9182,
          "id_cenario": 5379,
          "ds_modelo": "informes",
          "dt_data_prev": 1627009200,
          "ck_rmv": false,
          "dt_inicio": 1627959600,
          "dt_fim": 1642561200,
          "preview": null,
          "uuid": "6254f122-ec01-11eb-bfbe-02001700dcdd"
        }
      ]
    }
  ],
  "user": {
    "id": 1,
    "emails": [],
    "phones": [],
    "empresa": {}
  }
}
```