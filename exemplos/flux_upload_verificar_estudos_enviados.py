from ee_ampere_consultoria.produtos.flux import FluxUpload
import hashlib

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"

# EXECUÇÃO --------------------------------------------------------------
flux = FluxUpload(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)
lista_estudos_enviados = flux.get_list()
print(lista_estudos_enviados)