from ee_ampere_consultoria.produtos.meteorologia import Meteorologia
from ee_ampere_consultoria.produtos.meteorologia import ModelosDataStore
from ee_ampere_consultoria.produtos.meteorologia import NiveisAtm
from ee_ampere_consultoria.produtos.meteorologia import VariaveisMeteorologicas
from ee_ampere_consultoria.produtos.meteorologia import Runtime
from ee_ampere_consultoria.produtos.meteorologia import  RemocaoVies
from ee_ampere_consultoria.produtos.meteorologia import Membro
import datetime
import hashlib


# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
ontem = hoje - datetime.timedelta(days=1)

di = hoje + datetime.timedelta(days=1)
df = hoje + datetime.timedelta(days=15)

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"

met = Meteorologia(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)
r = met.get_images()


f = open("resultados_meteorologia_datastore.zip", "wb")
f.write(r)
f.close()
print("done")