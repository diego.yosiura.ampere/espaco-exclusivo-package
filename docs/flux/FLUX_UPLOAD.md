# FLUX Upload
>>>
Metodos e procedimentos para acesso à ferramenta FLUX Upload.
>>>

## Importação
```python
from ee_ampere_consultoria.produtos.flux import FluxUpload
```

## Método get_list()
>>>
Retorna a lista de processos enviados pelo cliente. 
>>>

```python
fl = FluxUpload('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
print(fl.get_list())
```
`output`
```json
[
  {"id":"id", "value":2541},
  {"id":"ds_nome_estudo", "value":"TESTE"},
  {"id":"id_empresa", "value":1},
  {"id":"id_user", "value":40},
  {"id":"dt_inicio", "value":1624192230},
  {"id":"dt_fim", "value":1625229030},
  {"id":"dt_last_execution", "value":1624071617},
  {"id":"dt_last_response", "value":1624071989},
  {"id":"vl_execution_counter", "value":1},
  {"id":"ready", "value":true},
  {"id":"ck_internal_error", "value":false},
  {
  	"id":"cenarios",
  	"value":[
  		{
  			"id":4679,
  			"id_estudo":2541,
  			"ds_nome":"CENARIO-TESTE",
  			"blocos": [
  				{
  					"id":7901,
  					"id_cenario":4679,
  					"ds_modelo":"cf295cdf111ddc00fafef9a9ff291146/USUARIO/flux_arquivos_upload_2822444a-d028-11eb-b1e1-02001700dcdd.zip",
  					"dt_data_prev":1624071615,
  					"ck_rmv":false,
  					"dt_inicio":1624192230,
  					"dt_fim":1625229030
				}
        	]
      }
    ]
  }
]
```

## Método get_download_link(self, caso_id: int) -> str
>>>
Retorna o link temporário para download do arquivo de resultados.
>>>

```python
fl = FluxUpload('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.get_download_link(2541))
```

## Método upload_file(self, ds_nome_estudo: str, dt_inicio: datetime, dt_fim: datetime, file_path: str) -> str
```python
fl = FluxUpload('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.upload_file("TESTE-API-UPLOAD", datetime(2021, 7, 23), datetime(2021, 7, 30), 'saida.zip'))
```
`output`
```json
{
  "id": 2896,
  "id_empresa": 1,
  "id_user": 1,
  "ds_nome_estudo": "TESTE-API-UPLOAD",
  "dt_inicio": 1627009200,
  "dt_fim": 1627614000,
  "dt_last_execution": null,
  "dt_last_response": null,
  "vl_execution_counter": -1,
  "ready": true,
  "uuid": "10930fba-ebeb-11eb-8dcb-02001700dcdd",
  "ck_internal_error": false,
  "ck_is_file": true,
  "ck_is_nt": false,
  "cenarios": [
    {
      "id": 5375,
      "id_estudo": 2896,
      "ds_nome": "CENARIO-TESTE-API-UPLOAD",
      "uuid": "10939e58-ebeb-11eb-8dcb-02001700dcdd"
    }
  ],
  "user": {
    "id": 1
  }
}
```