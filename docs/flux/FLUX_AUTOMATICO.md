# FLUX Automático
>>>
Metodos e procedimentos para acesso à ferramenta FLUX automático.
>>>

## Importação
```python
from ee_ampere_consultoria.produtos.flux import FluxAutomatico
```

## Método get_list()
>>>
Retorna uma lista contendo todos os arquivos disponíveis para Download. 
>>>

```python
fl = FluxAutomatico('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                    proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
print(fl.get_list())
```
`output`
```json
{
  "ACOMPH20210723": {
    "20210723": [
      "NOVAPREVC-ONS-OFICIAL-NT00752020.zip",
      "ETA40-RMV.zip"
    ]
  },
  "ACOMPH20210721": {
    "20210722": [
      "ZERO.zip",
      "PREVC-NT00752020-COMBINACAO-SOMENTE.zip",
      "NOVAPREVC-ONS-OFICIAL-NT00752020.zip",
      "GEM-RMV.zip",
      "GEFS-RMV.zip",
      "GEFS-35DIAS-PREV-20210721-RMV.zip",
      "ETA40-RMV.zip",
      "ECMWF-RMV.zip",
      "ECMWF-ENSEMBLE-RMV.zip",
      "CLIMATOLOGIA.zip",
      "CFSV2-TROPICAL.zip",
      "CFSV2-TROPICAL-RMV.zip",
      "CFSV2-INFORMES.zip",
      "CFSV2-INFORMES-RMV.zip",
      "AMPERE-ENSEMBLE-RMV.zip",
      "ACCESSG-RMV.zip"
    ]
  }
}
```

## Método download(self, acomph: str, data_prev: str, modelo: str, file_name: str = '') -> bytes
>>>
Baixa o arquivo contendo as informações do AcompH escolhido.

O argumento `file_name` é opcional se omitido, o método retorna uma cadeia de bytes.
>>>

```python
fl = FluxAutomatico('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.download('ACOMPH20210722', '20210723', 'PREVC-NT00752020-COMBINACAO-SOMENTE', 'saida.zip'))
```