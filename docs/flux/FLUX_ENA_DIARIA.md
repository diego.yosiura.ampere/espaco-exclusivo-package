# FLUX GT
>>>
Metodos e procedimentos para acesso à ferramenta FLUX ENA Diária.
>>>

## Importação
```python
from ee_ampere_consultoria.produtos.flux import FluxENADiaria
```

## Método get_simulacoes()
>>>
Retorna um dicionário com o os dados necessárioa para consultar os valores de ENA diária. 
>>>

```python
fl = FluxENADiaria('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                   proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
print(fl.get_simulacoes())
```
`output`
```json
{
  "2021-12-10": {
    "NOVAPREVC-ONS-OFICIAL-NT00752020": {
      "PRE-ACOMPH-PRE-PSAT": [
        "SUB_SUDESTE",
        "SUB_SUL",
        "SUB_NORDESTE",
        "SUB_NORTE",
        "REE_BMONTE",
        "REE_IGUACU",
        "REE_ITAIPU",
        "REE_MADEIRA",
        "REE_MAN-AP"
      ]
    },
    "ECMWF-ENSEMBLE-RMV": {
      "PRE-ACOMPH-PRE-PSAT": [
        "SUB_SUDESTE",
        "SUB_SUL",
        "SUB_NORDESTE",
        "SUB_NORTE",
        "REE_BMONTE",
        "REE_IGUACU",
        "REE_ITAIPU",
        "REE_MADEIRA",
        "REE_MAN-AP"
      ]
    }
  }
}
```

## Método get_ena(self, run_time: str, modelo: str, versao: str, subdivisao: str) -> bytes
>>>
Retorna uma lista de dicionários contendo as informaçÕes de data e valor da ENA.

Todos dos argumentos são obrigatórios e a combinação deve ser obtida pelo método `get_simulacoes`.
>>>

```python
fl = FluxENADiaria('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.get_ena('2021-12-10', 'NOVAPREVC-ONS-OFICIAL-NT00752020', 'PRE-ACOMPH-PRE-PSAT', 'SUB_SUDESTE'))
```

`output`
```json
[
  {"fcast": 1639063830, "ena": 33808.3176},
  {"fcast": 1639150230, "ena": 33408.84612}
]
```