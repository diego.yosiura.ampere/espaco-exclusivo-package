from ee_ampere_consultoria.produtos.meteorologia import Meteorologia
from ee_ampere_consultoria.produtos.meteorologia import BodyComparador
from ee_ampere_consultoria.produtos.meteorologia import TipoComparacao
from ee_ampere_consultoria.produtos.meteorologia import TipoPeriodoComparacao
from ee_ampere_consultoria.produtos.meteorologia import Modelos
import datetime
import hashlib
import base64


# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
ontem = hoje - datetime.timedelta(days=1)
d1 = hoje + datetime.timedelta(days=1)
d10 = hoje + datetime.timedelta(days=10)
d14 = hoje + datetime.timedelta(days=14)


# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"


# EXEMPLO 1) PREV MODELO 1 vs PREV MODELO 2 -----------------------------
met = Meteorologia(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)

formulario_comparador = BodyComparador()
formulario_comparador.set_modelo_confrontante(Modelos.NPREVC)
formulario_comparador.set_data_previsao_confrontante(ontem)
formulario_comparador.set_modelo_confrontante_rmv(True)

formulario_comparador.set_tipo_comparacao(TipoComparacao.PREVISAO_VS_PREVISAO)

formulario_comparador.set_modelo_base(Modelos.ETA)
formulario_comparador.set_data_previsao_base(ontem)
formulario_comparador.set_modelo_base_rmv(True)

formulario_comparador.set_tipo_de_periodo(TipoPeriodoComparacao.ACUMULADO)
formulario_comparador.set_periodo_analise_acumulada(d1, d10)

dict_figuras = met.comparar(formulario_comparador)
if not dict_figuras:
    raise("""Não foi possível gerar as figuras. 
    Verifique as datas e os modelos escolhidos.""")

for k, v in dict_figuras.items():
    res = base64.b64decode(v.split(',')[1])
    print(f"Salvando figura {k}.png")
    with open(f'{k}.png', "wb") as f:
        f.write(res)


# EXEMPLO 2) PREV ATUAL MODELO 1 vs PREV ONTEM MODELO 1 ----------------4
met = Meteorologia(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)

formulario_comparador = BodyComparador()
formulario_comparador.set_modelo_confrontante(Modelos.NPREVC)
formulario_comparador.set_data_previsao_confrontante(hoje)
formulario_comparador.set_modelo_confrontante_rmv(True)

formulario_comparador.set_tipo_comparacao(TipoComparacao.PREVISAO_VS_PREVISAO)

formulario_comparador.set_modelo_base(Modelos.NPREVC)
formulario_comparador.set_data_previsao_base(ontem)
formulario_comparador.set_modelo_base_rmv(True)

formulario_comparador.set_tipo_de_periodo(TipoPeriodoComparacao.ACUMULADO)
formulario_comparador.set_periodo_analise_acumulada(d1, d14 - datetime.timedelta(days=1))
# data máxima de previsão ontem = data máxima de previsão atual - 1

dict_figuras = met.comparar(formulario_comparador)
if not dict_figuras:
    raise("""Não foi possível gerar as figuras. 
    Verifique as datas e os modelos escolhidos.""")

for k, v in dict_figuras.items():
    res = base64.b64decode(v.split(',')[1])
    print(f"Salvando figura {k}.png")
    with open(f'{k}.png', "wb") as f:
        f.write(res)



# EXEMPLO 3) PREV ONTEM vs OBS HOJE -------------------------------------
met = Meteorologia(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)

formulario_comparador = BodyComparador()
formulario_comparador.set_modelo_confrontante(Modelos.NPREVC)
formulario_comparador.set_data_previsao_confrontante(ontem)
formulario_comparador.set_modelo_confrontante_rmv(True)

formulario_comparador.set_tipo_comparacao(TipoComparacao.PREVISAO_VS_OBSERVADA)

formulario_comparador.set_tipo_de_periodo(TipoPeriodoComparacao.ACUMULADO)
formulario_comparador.set_periodo_analise_acumulada(d1, d1)

dict_figuras = met.comparar(formulario_comparador)
if not dict_figuras:
    raise("""Não foi possível gerar as figuras. 
    Verifique as datas e os modelos escolhidos.""")

for k, v in dict_figuras.items():
    res = base64.b64decode(v.split(',')[1])
    print(f"Salvando figura {k}.png")
    with open(f'{k}.png', "wb") as f:
        f.write(res)