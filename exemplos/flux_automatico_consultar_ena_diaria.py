from ee_ampere_consultoria.produtos.flux import FluxENADiaria
import datetime
import hashlib

# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"


# EXECUÇÃO --------------------------------------------------------------
flux = FluxENADiaria(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)


# PROCESSANDO ENA DIÁRIA DE UM ÚNICO CENÁRIO ----------------------------
runtime = datetime.datetime.strftime(hoje, "%Y-%m-%d")
modelo = "NOVAPREVC-ONS-OFICIAL-NT00752020"
versao = "PRE-ACOMPH-PRE-PSAT"
subdivisao = "SUB_SUDESTE"
r = flux.get_ena(runtime, modelo, versao, subdivisao)


# COMPILANDO RELAÇÃO COM CENÁRIOS DISPONÍVEIS ---------------------------
lista_estudos = flux.get_simulacoes()
lista_datas_com_simulacoes = sorted(lista_estudos.keys())


print("aqui")