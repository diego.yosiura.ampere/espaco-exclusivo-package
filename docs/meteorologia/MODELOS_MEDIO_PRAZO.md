# Modelos de Médio Prazo

## CFSv2 - MEMBROS
>>>
O Climate Forecast System, version2 – CFSv2 é um modelo de previsão climática que representa a interação global entre as massas oceânicas, terrenas e atmosféricas.

Desenvolvido no centro americano NCEP/NOAA, apresenta uma grade de representação espacial com resolução horizontal de aproximadamente 100 km e 64 camadas verticais. As condições iniciais para a simulação climática são obtidas praticamente em tempo real a partir do sistema de assimilação de dados climáticos CDAS, permitindo a produção de 16 previsões distintas ao longo do dia. Dentre elas, quatro compreendem um horizonte de previsão de 9 meses, três compreendem um horizonte de previsão de 3 meses e nove contemplam um horizonte de apenas 45 dias. Denomina-se membro cada previsão individual.

Os mapas de previsão climática de médio/longo prazo apresentados nesta página são baseados no acompanhamento da chuva observada do CPTEC/INPE e resultados publicados pelo centro americano NCEP/NOAA para as projeções do modelo CFSv2, conforme o horizonte de previsão indicado. Apresenta-se os resultados compilados dos quatro membros com previsão de mais longo prazo, com inicializações às 0000 UTC, 0006 UTC, 0012 UTC e 00018 UTC.

Para mais informações, consulte a página do [NCEP/NOAA](https://cfs.ncep.noaa.gov/).
>>>


## CFSv2 - TROPICAL
>>>
Este produto apresenta uma composição média do acompanhamento da chuva observada do CPTEC/INPE e resultados publicados pelo centro americano NCEP/NOAA das 12 previsões mais recentes do modelo CFSv2, retiradas das simulações inicializadas às 0000 UTC, 0006 UTC, 0012 UTC e 00018 UTC dos últimos 3 dias, conforme o horizonte de previsão indicado.
>>>


## CFSv2 – INFORME
>>>
Este produto apresenta uma composição média do acompanhamento da chuva observada do CPTEC/INPE e resultados das previsões numéricas e climáticas do tempo seguindo a metodologia adotada internamente na AMPERE. Para os 10 primeiros dias de previsão se utiliza o resultado da previsão conjunta dos modelos ETA40 e GEFS, com tratamento de viés, conforme especificado na nota técnica NT-0156-2019-R10 do ONS. Para o intervalo entre o 11º e o 15º dia de previsão se utilizada da previsão conjunta dos modelos GEFS e a composição CFSv2 dos membros inicializados às 0000 UTC dos últimos 7 dias, com pesos iguais. Do 16º dia em diante se utiliza somente a composição CFSv2 dos membros inicializados às 0000 UTC dos últimos 7 dias.
>>>