# File Viewer
>>>
Contém os métodos e procedimentos para acessar os arquivos exclusivos dos cliente Ampere Consultoria.

O `File Viewer` contém os arquivos de relatórios periódicos e arquivos publicados para um cliente específico
ou para um grupo de clientes.

Arquivos para produtos específicos como [Meteorologia](../meteorologia/METEOROLOGIA.md) ou [Flux](../flux/FLUX.md) 
devem ser recuperados utilizando os métodos e procedimentos disponíveis em cada um dos produtos.
>>>

## Instalação e Importanção
>>>
O pacote pode ser instalado usando o gerenciador `pip` do Python `pip install espaco_exclusivo_package`.

Para cada produto, deve-se utilizar o objeto dedicado ao produto para que seja possível acessar seus métodos e propriedades.
>>>
```python
from ee_ampere_consultoria.produtos.file_viewer import FileViewer
```

## Instância
>>>
O objeto `FileViewer` precisa ser instanciado antes de se utilizar, ao 
utilizar a instância do objeto, garantimos que as chaves sejam renovadas sempre que necessário,
evitando requisições desnecessárias ao servidor.

Assinatura do objeto:

`FileViewer`(*username* _str_, *password* _str_, *token* _str_`[`, *user* _User_`]`)

- username (string) - Nome de usuário utilizado para efetuar o login no *Espaço Exclusivo - Ampere Consultoria*.
- password (string) - Senha de acesso do usuário utilizado para efetuar o login no *Espaço Exclusivo - Ampere Consultoria*, a senha deve ser enviada como um _hash_ `md5``.
- token (string) - Token de acesso, o token deve ser solicitado via e-mail [ampere@ampereconsultoria.com.br](mailto:ampere@ampereconsultoria.com.br).
- user (User) - Opcional, contem as iformações especificas do usuário.
>>>
```python
fv = FileViewer('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
```

### Método get_list() -> dict
>>>
Retorna um dicionário contendo todos os arquivos contidos nos diretórios dos clientes e produtos contratados.

`FileViewer`->get_list() returns `dict`
>>>
```python
dados = fv.get_list()
print(dados)
```
`output:`
```python
dados = {
  "dir-name": {                          # Nome do diretório pai
    "id": "xxx-xxx-xxx",                 # Identificador único
    "type": "D",                         # Tipo: [D] - Diretório | [F] - Arquivo
    "name": "XXXXXX",                    # Nome do diretório
    "children": {                        # Arquivos e diretórios filhos
      "file-01-hash": {
        "type": "F",                     # Tipo: [D] - Diretório | [F] - Arquivo
        "id": "XXXXXX",                  # Identificador único
        "size": 10005,                   # Tamanho do arquivo em Bytes
        "name": "Nome do Arquivo.xlsx",  # Nome do arquivo com extensão
        "extension": "xlsx",             # Extensão do arquivo
        "modified": 1598666519.4391475   # Data de modificação no formato UNIX Timestamp
      }
    }
  }
}
```

### Método download(file_id: str, file_name: str) -> bytes
>>>
Faz o download de um arquivo usando o identificador retornado no método get_list().

O segundo argumento é opcional, quando não especificado, o método retorna o arquivo como uma cadeira de bytes.

`FileViewer`->download(*file_id* _str_`[`, *file_name* _str_`]`) returns `bytes`

- file_id (string) - Identificador Únido do arquivo, deve ser o mesmo valor retornado no método `FileViewer`->get_list()
- file_name (string) - (Opcional) Arquvio de destino do download.
>>>

```python
## Faz o download do arquivo
fv.download(dados['dir-name']['children']['file-01-hash']['id'], 'saida.xlsx')

## Retorna o arquivo como uma cadeira de bytes
arquivo = fv.download(dados['dir-name']['children']['file-01-hash']['id'])
```