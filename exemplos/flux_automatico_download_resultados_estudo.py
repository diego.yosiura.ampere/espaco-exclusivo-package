from ee_ampere_consultoria.produtos.flux import FluxAutomatico
import hashlib


# DEFINIÇÃO DO ESTUDO A SER BAIXADO -------------------------------------
data_prev = "20220511"
acomph = "ACOMPH20220510"
psat = "20220511-PSAT"
modelo = "ICON-RMV"


# CREDENCIAIS -----------------------------------------------------------
USERNAME = "username_exemplo"
senha = "senha_exemplo"
MD5_PASSWORD_HASH = hashlib.md5(senha.encode("utf-8")).hexdigest()
USER_ACCESS_TOKEN = "USER-ACCESS-TOKEN-EXEMPLO"


# EXECUÇÃO --------------------------------------------------------------
flux = FluxAutomatico(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)

force_download = False
horario_ultimo_arquivo_ja_baixado = (
    "timestamp_horario_exemplo"  # Pegar do zip já baixado ou do banco interno
)
latest_results = flux.verify_last_results(
    acomph, data_prev, modelo
)  # Caso queira pegar os resultados mais recentes, não enviar nada.
realizar_download = force_download or latest_results > horario_ultimo_arquivo_ja_baixado
if realizar_download:
    r = flux.download(acomph, data_prev, modelo, "resultados_automatico_exemplo.zip")
