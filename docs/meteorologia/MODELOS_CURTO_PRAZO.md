# Modelos de Curto Prazo

## ETA40
>>>
O `ETA40` representa o estado da arte da modelagem atmosférica para fins operacionais e de pesquisa em previsões numéricas do tempo. Consiste em uma evolução do equacionamento do modelo `HIBU`, desenvolvido durante os anos 70 no _Instituto Hidrometeorológico da Universidade de Belgrado_ com referências no trabalho desenvolvido por *Mesinger e Janjic (1974)*.

O código foi atualizado para integrar o esquema de advecção horizontal proposto por *Janjic (1984)* e reescrito para contemplar o uso da variável vertical `ETA` (), de onde derivou o seu nome atual.

Foi implementado em versões e adaptações diversas em países como Algeria, Argentina, Bélgica, Camarões, China, Costa Rica, Chipre, República Checa, Dinamarca, Egito, Finlândia, Alemanha, Grécia, Islândia, Índia, Israel, Itália, Malta, Tunísia, Turquia, Peru, Filipinas, Sérvia, Montenegro, África, Espanha, Suécia, Estados Unidos e também no Brasil.

A versão do modelo `ETA` que roda no [CPTEC/INPE](http://etamodel.cptec.inpe.br/) e disponibilizada ao público e ao `Operador Nacional do Sistema` _ONS_ considera uma atmosfera hidrostática, representada por uma grade com resolução horizontal de 40 km e 38 camadas verticais, cobrindo uma extensão territorial que engloba praticamente toda a América do Sul.

As previsões são fornecidas duas vezes ao dia, uma com condição inicial às 0000 UTC e outra às 1200 UTC, sendo a versão das 0000 UTC aquela utilizada no planejamento de curto prazo do setor elétrico.

Para mais informações, consulte a página do [CPTEC/INPE](http://etamodel.cptec.inpe.br/).
>>>
 
## GEFS
>>>
O `Global Ensemble Forecast System` é o resultado da composição de 21 previsões numéricas do tempo distintas.

Denomina-se membro cada previsão individual e _ensemble_ a composição média dada pela compilação de todos os membros. As previsões utilizadas para compor o _ensemble_ são provenientes de rodadas do modelo `Global Forecast System`, produzido pelo centro americano [NCEP/NOAA](https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/global-ensemble-forecast-system-gefs).

A composição das diversas previsões retrata uma tentativa de se quantificar o nível de incerteza associado à previsão numérica do tempo a partir da avaliação de diversos resultados, cada qual produzido a partir de uma mesma base de dados observados, mas com alterações mínimas, ou perturbações, no estado inicial de algumas variáveis meteorológicas relevantes.

A intenção é compreender a dimensão das variações nas saídas do modelo em função da propagação destas perturbações no sistema atmosférico.

O modelo `GEFS` simula o sistema atmosférico ao redor de todo o globo terrestre, fornecendo previsões para todos os 21 membros e para o _ensemble_ quatro vezes ao dia, com condições iniciais às 0000 UTC, 0006 UTC, 0012 UTC e 00018 UTC.

A versão utilizada no planejamento de curto prazo do setor elétrico possui resolução horizontal 1, aproximadamente 100 km, e corresponde à inicialização das 0000 UTC.

Para mais informações, consulte a página do [NCEP/NOAA](https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/global-ensemble-forecast-system-gefs).
>>>


## PREVISÃO CONJUNTA
>>>
A técnica de previsão por conjunto, ou _ensemble_, surgiu como uma forma de minimizar o afastamento entre previsões determinísticas de precipitação e a realidade dos acumulados observados. A técnica consiste na compilação de previsões modelos distintos e/ou previsões de um mesmo modelo com diferenças pontuais na parametrização, ou inicialização.

Uma análise estatística sobre um histórico suficientemente longo é capaz de apontar qual modelo possui melhor desempenho para uma dada região e horizonte de previsão, determinando fatores ponderadores em função deste resultado.

A previsão conjunta apresentada aqui contempla os resultados publicados pelo modelo ETA40 do [CPTEC/INPE](http://cptec.inpe.br/) e GEFS do [NCEP/NOAA](https://www.ncdc.noaa.gov/data-access/model-data/model-datasets/global-ensemble-forecast-system-gefs), ambos com inicialização às 0000 UTC. Os fatores de ponderação aplicados a cada modelo, bacia hidrográfica e dia de previsão foram definidos em estudo dedicado pelo próprio ONS, conforme especificado na nota técnica NT-0156-2019-R10.

Também é possível consultar nesta mesma NT a metodologia e parâmetros para a remoção de viés das previsões meteorológicas de ambos os modelos.
>>>


## ARPEGE
>>>
O modelo numérico global de previsão do tempo ARPEGE (`Action de Pesquisa Petite Echelle Grande Echelle`) é uma ferramenta essencial para a previsão operacional do tempo na `Météo France`. É totalmente integrado ao software ARPEGE-IFS desenvolvido e mantido em colaboração com o `ECMWF`.

Possui a maioria dos atributos necessários para previsões numéricas operacionais, o que garante total consistência entre os cálculos realizados durante a análise, o modelo e as etapas de pós-processamento. À medida que uma nova versão é implementada a cada nove meses, o código está evoluindo continuamente para obter o melhor dos supercomputadores, assimilar dados de novos sistemas de observação e melhorar os componentes do modelo de previsão.

A vida útil do modelo `ARPEGE` vai muito além de qualquer de seus antecessores, sendo utilizada há mais de 23 anos, contra o recorde anterior de 8 anos e 7 meses para o modelo `Péridot`.

As condições iniciais do modelo `ARPEGE` são baseadas em uma assimilação variacional 4-dimensional (4D-Var), que incorpora uma quantidade muito grande e variada de observações convencionais (rádios, medições de aviões, estações terrestres, navios, bóias, etc.) e também de sensoriamento remoto (ATOVS, SSMI / S, AIRS, IASI, CRIS, ATMS, SEVIRI, GPS sol, satélite GPS, SATOB, difusores, etc ...).

O `ARPEGE` usa um conjunto de equações primitivas com um truncamento espectral triangular na horizontal, com uma resolução horizontal variável, com uma representação de elementos finitos na vertical e uma coordenada vertical híbrida de “pressão sigma”. Também utiliza um esquema semi-implícito temporal semi-implícito em dois tempos. A resolução horizontal do modelo `ARPEGE` é de cerca de 7,5 km na França e 37 km no resto do globo.

Possui 105 níveis verticais, com o primeiro nível a 10m acima da superfície e o nível superior a cerca de 70km. O time step é de 360 segundos.

O modelo possui todas as parametrizações atmosféricas básicas e conhecimentos dos processos da superfície provenientes do `ISBA` (DOUVILLE, 1998). A convecção profunda é representada pelo esquema de fluxo de massa com detranhamento como proposto por Bougeault (1985). A formação de nuvem estratiforme e a convecção rasa são avaliadas através do método estatístico descrito em Ricard e Royer (1993).

A convecção leva em conta a dependência vertical do processo de entranhamento do ar (TERRAY, 1998) e a condição de fechamento é baseada no critério de convergência de umidade. O pacote radiativo segue o esquema de Fouquart e Morcrette (MORCRETTE, 1990).

Para mais informações, consulte a página do [Centre National De La Recherche Scientifique](https://www.umr-cnrm.fr/spip.php?article121).
>>>


## ECMWF (European Centre for Medium-Range Weather Forecasts)
>>>
É uma previsão de conjunto, consiste em cerca de 51 previsões separadas e ativadas a partir do mesmo horário de início. As condições de partida para cada membro do conjunto são ligeiramente diferentes e os valores dos parâmetros físicos usados também diferem ligeiramente.

As diferenças entre esses membros do conjunto tendem a aumentar à medida que as previsões progridem, ou seja, à medida que o lead time de previsão aumenta. O `ECMWF` fornece previsões globais, análises de clima e conjuntos de dados específicos, projetados para atender a diferentes requisitos do usuário. As previsões operacionais do `ECMWF` visam mostrar como o clima tem maior probabilidade de evoluir.

Para fazer isso, o Centro produz um conjunto de previsões. Individualmente, são descrições completas da evolução do clima. Coletivamente, eles indicam a probabilidade de uma variedade de cenários climáticos futuros.

As previsões estão baseadas nas: observações (condições iniciais), informações prévias sobre o sistema terrestre e modelos de alta resolução do `ECMWF`. O centro disponibiliza quatro previsões por dia (horários sinóticos 00/06/12/18).

Para mais informações, [consulte](https://www.ecmwf.int/en/forecasts/documentation-and-support#Atmospheric).
>>>


## GEM (Global Environmental Multiscale model)
>>>
Sistema integrado de previsão e assimilação de dados desenvolvido com o objetivo de atender as necessidades operacionais e previsíveis de previsão do tempo do Canadá.

No entanto, o modelo é global, assim os fenômenos de grande escala como ondas longas são tratadas adequadamente para assimilação de dados e previsão de longo alcance. O modelo possui um recurso de resolução variável, de modo que é possível usar uma malha de latitude / longitude de resolução uniforme ou uma malha de resolução variável que usa um sistema de coordenadas com um subdomínio de alta resolução que pode ser localizado sobre qualquer parte do globo. O espaçamento da grade aumenta suavemente para atingir seu valor máximo nos polos.

A dinâmica do modelo `GEM` operacional é formulada em termos das equações hidrostáticas primitivas seguindo a coordenada vertical de pressão. A discretização do tempo é um esquema semi-lagrangiano implícito em dois níveis. A discretização espacial é uma formulação de ponto de grade de Galerkin em uma grade C de Arakawa na horizontal (lat-lon) e uma discretização vertical sem interrupções. A malha horizontal pode ter resolução uniforme ou variável e, além disso, pode ser girada arbitrariamente, a malha vertical também é variável.

O modelo `GEM` operacional possui parametrizações físicas que atualmente incluem:

- Radiação solar e infravermelha interativa com vapor de água, dióxido de carbono, ozônio e nuvens,
- Previsão da temperatura da superfície sobre o solo com o método de força-restauração,
- Turbulência na camada limite planetária, coeficientes de difusão com base na estabilidade e energia cinética turbulenta,
- Camada superficial baseada na teoria da similaridade de Monin-Obukhov,
- Esquema de convecção superficial (não precipitante),
- Esquema de convecção profunda do tipo Kuo (sistema de previsão global),
- Esquema de convecção profunda do tipo Fritsch – Chappell (sistema de previsão regional),
- Esquema de condensação Sundqvist (1978) para precipitação estratiforme,
- Arrasto de onda.

Para mais informações, [consulte](https://collaboration.cmc.ec.gc.ca/science/rpn/gef_html_public/index.html).
>>>


## UKMO
>>>
UKMO ou UM (`Met Office Unified Model`) é um modelo de circulação global com as mais recentes configurações científicas da superfície terrestre do simulador Joint UK Land Environment Simulator (JULES) desenvolvido para uso em escalas de tempo e clima. Conhecido também como Global Atmosphere (GA7.0) e Global Land (GL7.0), inclui desenvolvimentos incrementais e metas que abordam quatro erros críticos identificados nas configurações anteriores: precipitação excessiva sobre a Índia, desvios quentes e úmidos nas camada de tropopausa, fonte de não conservação de energia no esquema de advecção e desvios excessivos da radiação superficial sobre o oceano no Hemisfério Sul.

Inclui duas novas parametrizações: `UK Chemistry and Aerosol` (UKCA) modo GLOMAP (`Modelo Global de Processos em Aerossol`) e o esquema de aerossóis e neve de várias camadas, que melhoram a fidelidade da simulação e foram necessários para inclusão na Atmosfera Global antes da 6ª Intercomparação do Modelo Acoplado Projeto (CMIP6).
Para mais informações, [consulte](https://www.geosci-model-dev.net/12/1909/2019/gmd-12-1909-2019.pdf)
>>>


## ACCESS (Australian Community Climate and Earth-System Simulator )
>>>
Modelo desenvolvido e testado continuamente pela equipe de pesquisa do `Departamento de Pesquisa e Desenvolvimento Australiano Bureau`. É baseado em parametrizações do `UKMO` (`UK Meteorological Office's Unified Model`). Possui domínio global com, aproximadamente, 25km de resolução.

O time step é de seis horas, com rodadas disponibilizadas às 00Z, 0645Z, 12Z e 1845Z .O horizonte de previsão é de dez dias.

Para mais informações, [consulte](http://www.bom.gov.au/australia/charts/about/about_access.shtml).
>>>


## CFSv2
>>>
O `CFSv2` simula o padrão espacial da precipitação anual e sazonal semelhante aos das análises, mas, em geral, subestima a precipitação sobre o continente em cerca de 1-2 mm/dia e superestima na Zona de Convergência Intertropical (ZCIT).

Autores também concluem que o `CFSv2` é hábil em reproduzir a variação sazonal da precipitação na América do Sul e, portanto, suas variáveis simuladas (temperatura, vento etc.) podem ser utilizadas como condições inicial e de fronteira em Modelos de Circulação Regional.

Silva et al. (2014) analisaram previsões do `CFSv2` no período de dez-jan-fev e jun-jul-ago de 1983 a 2010 sobre a AS. Entre os resultados foi verificado que o CFSv2 representa melhor a precipitação sobre o continente do que sobre o oceano.

[Fonte](http://dx.doi.org/10.1590/0102-7786332001)
>>>


## WRF
>>>
Noções da parametrização `WRF`: PARAMETRIZAÇÕES CONVECTIVAS NO MODELO WRF E SUA RELAÇÃO COM A PRECIPITAÇÃO DURANTE CICLOGÊNESES NO SUDESTE DA AMÉRICA DO SUL.

[Link](http://ftp.cptec.inpe.br/modelos/produtos/WRF/ams_05km/web/2020/01/24/)
>>>