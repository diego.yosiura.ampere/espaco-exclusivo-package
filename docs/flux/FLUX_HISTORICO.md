# FLUX Histórico
>>>
Metodos e procedimentos para acesso à ferramenta FLUX Histórico.
>>>

## Importação
```python
from ee_ampere_consultoria.produtos.flux import FluxHistorico
```

## Método get_hist_data()
>>>
Retorna um dicionário com o histórico de MLT separado por Submercado, por Reservatório Equivalente e uma lista de arquivos ZIP com as saidas do SMAPH. 
>>>

```python
fl = FluxHistorico('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                   proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
print(fl.get_hist_data())
```
`output`
```json
{
    "status": 1,
    "code": 200,
    "type": 2,
    "title": null,
    "message": null,
    "data": {
        "submercado": {
            "sul": [
                ["subsistema", "sul" ],
                [null, "MAI/2021", "JUN/2021", "JUL/2021"],
                ["OBS", 29, 58, null],
                ["2000", null, 58, 47],
                ["VMED", null, 58, 48]
            ]
        },
        "ree": {
            "telespires": [
                ["ree", "teles pires"],
                [null, "MAI/2021", "JUN/2021", "JUL/2021"],
                ["OBS", 97, 80, null],
                ["2000", null, 80, 79],
                ["VMED", null, 80, 79]
            ]
        },
        "zip": [
            "VMED.zip",
            "SMAPH2020.zip",
            "SMAPH2019.zip"
        ],
        "observado": [
            [null, "MAI/2021", "JUN/2021"],
            [63, 66]
        ],
        "data_atualizacao": "Ultima atualização: 23/07/2021, 01:12:06"
    }
}
```

## Método download(self, zip_name: str, file_name: str = '') -> bytes
>>>
Baixa o arquivo contendo as informações do Arquivo zip escolhido, caso o nome seja TODOS, todos os arquivos serão baixados.

O argumento `file_name` é opcional e, se omitido, o método retorna uma cadeia de bytes.
>>>

```python
fl = FluxHistorico('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
print(fl.download('TODOS', 'saida_todos.zip'))
```