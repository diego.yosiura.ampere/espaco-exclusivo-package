# Meteorologia
>>>
Contém os métodos e procedimentos para acessar os produtos e serviços de meteorologia dos cliente Ampere Consultoria.

Para maiores informaçõe à respeito dos modelos de previsão, acess a área de Modelos:
- [Modelos de Curto Prazo](MODELOS_CURTO_PRAZO.md).
- [Modelos de Médio Prazo](MODELOS_MEDIO_PRAZO.md).
- [Modelos de Longo Prazo](MODELOS_LONGO_PRAZO.md).
>>>

## Instalação e Importanção
>>>
O pacote pode ser instalado usando o gerenciador `pip` do Python `pip install espaco_exclusivo_package`.

Para cada produto, deve-se utilizar o objeto dedicado ao produto para que seja possível acessar seus métodos e propriedades.

```python
# Objeto principal, contem os métodos e procedimentos para acessar os produtos
from ee_ampere_consultoria.produtos.meteorologia import Meteorologia

# Objeto que contem os métodos e procedimentos para validação das requisições de comparação.
from ee_ampere_consultoria.produtos.meteorologia import BodyComparador

# Enumerador de Prazos
from ee_ampere_consultoria.produtos.meteorologia import Prazo

# Enumerador de modelos disponíveis
from ee_ampere_consultoria.produtos.meteorologia import Modelos

# Enumerador de Tipos de comparação
from ee_ampere_consultoria.produtos.meteorologia import TipoComparacao

# Enumerador de Tipos dos periodos de comparação
from ee_ampere_consultoria.produtos.meteorologia import TipoPeriodoComparacao

# Enumerador de Número total de dias de previsão por modelo
from ee_ampere_consultoria.produtos.meteorologia import DiasModelos
```

### Método get_images(prazo: Prazo, dia: int, mes: int, ano: int, index: int) -> dict
>>>
> Retorna um dicionário com as imagens de acordo com o Prazo e o index.
> 
> `index`: dias da previsão 1 -> DiasModelos.modelo.value
>>>
```python
met = Meteorologia('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN',
                   proxy={'http': 'endereço-proxy', 'https': 'endereço-proxy'}, ssl_verify=True)
imagens = met.get_images(Prazo.CURTO_PRAZO, 20, 7, 2021, 1)
print(imagens)
```
`output:`
```json
{
  "ETA40": {
    "d01": {
      "prev": "data:image/png;base64,XXX",
      "diff": "data:image/png;base64,XXX",
      "rmv": "data:image/png;base64,XXX"
    }
  },
  "GEFS": {
    "d01": {
      "prev": "data:image/png;base64,XXX",
      "diff": "data:image/png;base64,XXX",
      "rmv": "data:image/png;base64,XXX"
    }
  }
}
```

### Método comparar(body_comparador: BodyComparador)
>>>
> Executa a comparação de dois modelos conforme os parâmetros informados no objeto BodyComparador
>>>

```python
met = Meteorologia('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
obj = BodyComparador()

obj.set_tipo_comparacao(TipoComparacao.PREVISAO_VS_PREVISAO)
obj.set_tipo_de_periodo(TipoPeriodoComparacao.DIARIO)
obj.set_periodo_analise_diaria(datetime(2021, 7, 22, 0, 0, 0, 0))

obj.set_modelo_base(Modelos.GFS)
obj.set_data_previsao_base(datetime(2021, 7, 21, 0, 0, 0, 0))
obj.set_modelo_base_rmv(True)

obj.set_modelo_confrontante(Modelos.COSMO)
obj.set_data_previsao_confrontante(datetime(2021, 7, 21, 0, 0, 0, 0))
obj.set_modelo_confrontante_rmv(True)

print(met.comparar(obj))
```
`output:`
```json
{
      "fig01": "data:image/png;base64,xxxx",
      "fig02": "data:image/png;base64,xxxx",
      "fig03": "data:image/png;base64,xxxx"
}
```

### Método chuva_observada(body_chuva_observada: BodyChuvaObservada)
>>>
> Faz a requisição da chuva observada para o período selecionado
>>>

```python
met = Meteorologia('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')
obj = BodyChuvaObservada()

body.set_tipo_de_periodo(TipoPeriodoComparacao.ACUMULADO)
body.set_periodo_analise_acumulada(datetime(2021, 1, 1, 0, 0, 0, 0), datetime(2021, 1, 10, 0, 0, 0, 0))

print(met.chuva_observada(obj))
```
`output:`
```json
{
      "fig01": "data:image/png;base64,xxxx"
}

```
### Método data_store_download(modelo: ModelosDataStore, data_previsao: date, nivel_atm: NiveisAtm, 
                               var_met: VariaveisMeteorologicas, runtime: int, rmv: bool, membro: str, 
                               data_inicial: date, data_final: date, file_name: str = '')
>>>
Permite o download dos arquivos em formato txt com as informacoes da previsao de interesse, caso disponivel.
> `modelo`: Obrigatorio, recomenda-se utilizar listagem em 
>           ee_ampere_consultoria.produtos.meteorologia.enum_modelos_data_store.py
>
> `data_inicial` e `data_final`: Opcionais - Se None, o método retorna todos os dados para a `data_previsao`.
>
> `nivel_atm`: Obrigatorio, recomenda-se utilizar listagem em 
>              ee_ampere_consultoria.produtos.meteorologia.enum_niveis_atm.py
>
> `var_met`: Obrigatorio, recomenda-se utilizar listagem em 
>            ee_ampere_consultoria.produtos.meteorologia.enum_variaveis_meteorologicas.py
>
> `runtime`: Obrigatorio, define o horário de inicializacao da previsao 
>            recomenda-se utilizar listagem em ee_ampere_consultoria.produtos.meteorologia.enum_runtime.py
>            valor usual = UTC00, outros horarios nao necessariamente disponiveis
>
> `rmv`: Obrigatorio, define se deseja os arquivos apos tratamento de eventos extremos
>        recomenda-se utilizar listagem em ee_ampere_consultoria.produtos.meteorologia.enum_rmvoptions.py
>
> `membro`: Obrigatorio, define se deseja resultados para um membro especifico
>           recomenda-se utilizar listagem em ee_ampere_consultoria.produtos.meteorologia.enum_membros.py
>           valor usual = 0 (membro de controle/ensemble)
>           especificacao de membro para previsao numerica do tempo = ['01','02',...,'50'] 
>           especificacao de membro para previsao probabilistica = ['p10','p25',...,'p90']
>
> `file_name`: Opcional - Se omitido o método retorna os bytes referentes ao arquivo ZIP.
>
> 
>
> Em um primeiro momento, somente se disponibilizam informacoes acerca da variavel meteorologica de precipitacao, 
> mas outras variaveis devem ser compiladas e disponibilizadas no futuro.
> A seguir, apresenta-se a lista com todas as variaveis e niveis atmosfericos planejados:
>
>    LISTA_VARIAVEIS_METEOROLOGICAS = {
>        "hgt"        # Geopotential Height [gpm];
>        "tmp"        # Temperature [K];
>        "tmax"       # Minimum Temperature [K];
>        "tmin"       # Maximum Temperature [K];
>        "prec"       # Total Precipitation [mm];
>        "rh"         # Relative Humidity [%];
>        "uwind"      # U-Component of Wind [m/s];
>        "vwind"      # V-Component of Wind [m/s];
>        "pressup"    # Pressure [Pa];
>        "tcc"        # Total Cloud Cover [%];
>        "dswrf"      # Downward Short-Wave Radiation Flux [W/m^2];
>    }
>
>	 LISTA_NIVEIS_ATMOSFERICOS = {
>		"single_level",
>		"200mb",
>        "500mb", 
>		"750mb", 
>		"850mb",
>		"1000mb"
>    }
>
>>>

```python
met = Meteorologia('USERNAME', 'MD5_PASSWORD_HASH', 'USER_ACCESS_TOKEN')

print(met.data_store_download(ModelosDataStore.GEM, date(2022, 1, 30), NiveisAtm.SingleLevel,
                             VariaveisMeteorologicas.PREC, Runtime.UTC00, RemocaoVies.TRUE, Membro.CONTROLE, None, None))
```