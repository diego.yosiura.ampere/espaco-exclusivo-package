from ee_ampere_consultoria.produtos.flux import FluxPersonalizado
import hashlib

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"

# EXECUÇÃO --------------------------------------------------------------
flux = FluxPersonalizado(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)
download_link = flux.get_download_link(15435)


# DOWNLOAD --------------------------------------------------------------
import requests
import os

dir_download = os.getcwd()
nome_zip_resultados = "resultados_flux_personalizado_exemplo.zip"
fullfilename = os.path.join(dir_download, nome_zip_resultados)
print("Download do arquivo: " + download_link)
try:
    r = requests.get(download_link, stream=True)
    with open(fullfilename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
                # f.flush() commented by recommendation from J.F.Sebastian
    # end with
except Exception as e:
    print("Problema no download")
    raise Exception(e)
# end try
