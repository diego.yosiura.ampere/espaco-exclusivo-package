from ee_ampere_consultoria.produtos.flux import FluxGT
import hashlib
import os


# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"

# LISTA DE RESULTADOS ---------------------------------------------------
flux = FluxGT(USERNAME, MD5_PASSWORD_HASH, USER_ACCESS_TOKEN)
resultados = flux.get_gt_data()

# DATA DE ATUALIZACAO ---------------------------------------------------
data_atualizacao = resultados['data_atualizacao']


# DADOS DE ENA MENSAL POR SUBSISTEMA/REE --------------------------------
ena_diaria_sub_sudeste = resultados['submercado']['sudeste']
ena_diaria_sub_sul = resultados['submercado']['sul']
ena_diaria_sub_nordeste = resultados['submercado']['nordeste']
ena_diaria_sub_norte = resultados['submercado']['norte']
# processar dados de ena mensal por subsistema #

ena_diaria_ree_sudeste = resultados['ree']['sudeste']
ena_diaria_ree_madeira = resultados['ree']['madeira']
ena_diaria_ree_telespires = resultados['ree']['telespires']
ena_diaria_ree_itaipu = resultados['ree']['itaipu']
ena_diaria_ree_parana = resultados['ree']['parana']
ena_diaria_ree_paranapanema = resultados['ree']['paranapanema']
ena_diaria_ree_sul = resultados['ree']['sul']
ena_diaria_ree_iguacu = resultados['ree']['iguacu']
ena_diaria_ree_nordeste = resultados['ree']['nordeste']
ena_diaria_ree_norte = resultados['ree']['norte']
ena_diaria_ree_belomonte = resultados['ree']['belomonte']
ena_diaria_ree_manausamapa = resultados['ree']['manausamapa']
# processar dados de ena mensal por ree #


# DOWNLOAD DE RESULTADOS ------------------------------------------------
dir_download = os.path.join(os.getcwd(),"ec46-multimembros")
if not os.path.isdir(dir_download): os.makedirs(dir_download)
for arquivozip in resultados['zip']:
    print("Download do cenário: {}".format(arquivozip))
    flux.download(arquivozip, "{}/{}".format(dir_download, arquivozip))
#end for