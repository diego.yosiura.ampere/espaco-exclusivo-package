Metadata-Version: 2.1
Name: espaco_exclusivo_package
Version: 0.1.33
Summary: Pacote de métodos usados para implementação da API de acesso aos produtos Ampere Consultoria
Home-page: https://gitlab.com/diego.yosiura.ampere/espaco-exclusivo-package.git
Author: Diego Isaac Haruwo Yosiura
Author-email: diego@ampereconsultoria.com.br
Project-URL: Bug Tracker, https://gitlab.com/diego.yosiura.ampere/espaco-exclusivo-package/-/issues
Classifier: Programming Language :: Python :: 3
Classifier: License :: OSI Approved :: MIT License
Classifier: Operating System :: OS Independent
Requires-Python: >=3.6
Description-Content-Type: text/markdown
License-File: LICENSE
Requires-Dist: requests
Requires-Dist: requests-toolbelt

# Espaço Exclusivo - Ampere Consultoria Empresarial
Contem métodos e procedimento para acessar os produtos e serviços oferecidos pela Ampere Consultoria.

## Acesso sem o pacote

> A documentação para acesso direto á API pode ser encontrada em: [API EE Postman](https://documenter.getpostman.com/view/5400288/SzKYMw7o)


## Instalação do pacote
>>>
O código fonte está disponível para download em: [Gitlab Ampere Consultoria](https://gitlab.com/diego.yosiura.ampere/espaco_exclusivo_package.git).
O pacote pode ser instalado usando o gerenciador `pip` do Python `pip install espaco_exclusivo_package`
>>>

### Descrição
> Este pacote fornece acesso de forma simplicada à API de produtos da Ampere Consultoria.

### Indice
1. [File Viewer](./docs/file_viewer/FILE_VIEWER.md)
2. [Meteorologia](./docs/meteorologia/METEOROLOGIA.md)
3. [FLUX](./docs/flux/FLUX.md)
    - [FLUX Automatico](./docs/flux/FLUX_AUTOMATICO.md)
    - [FLUX Historico](./docs/flux/FLUX_HISTORICO.md)
    - [FLUX GT](./docs/flux/FLUX_GT.md)
    - [FLUX EC46](./docs/flux/FLUX_EC46.md)
    - [FLUX Upload](./docs/flux/FLUX_UPLOAD.md)
    - [FLUX Personalizado](./docs/flux/FLUX_PERSONALIZADO.md)
    - [FLUX ENA Diária](./docs/flux/FLUX_ENA_DIARIA.md)
