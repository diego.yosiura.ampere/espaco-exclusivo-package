import datetime
import dateutils
import hashlib
from ee_ampere_consultoria.produtos.flux import FluxPersonalizado
from ee_ampere_consultoria.produtos.flux import BodyFluxPersonalizado
from ee_ampere_consultoria.produtos.flux import CenarioFluxPersonalizado
from ee_ampere_consultoria.produtos.meteorologia import Modelos
from ee_ampere_consultoria.produtos.flux import DimensoesAgrupamento

# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
ontem = hoje - datetime.timedelta(days=1)

# regrinha que determina previsao mais recente do EC46
if hoje.isoweekday() == 1: offset = 4
if hoje.isoweekday() == 2: offset = 1
if hoje.isoweekday() == 3: offset = 2
if hoje.isoweekday() == 4: offset = 3
if hoje.isoweekday() == 5: offset = 1
if hoje.isoweekday() == 6: offset = 2
if hoje.isoweekday() == 7: offset = 3
dprev_ec46 = hoje - datetime.timedelta(days=offset)

d1 = hoje + datetime.timedelta(days=1)
d14 = hoje + datetime.timedelta(days=14)
d15 = hoje + datetime.timedelta(days=15)

d46 = dprev_ec46 + datetime.timedelta(days=46)
d47 = dprev_ec46 + datetime.timedelta(days=47)

fim_m3 = datetime.date(hoje.year, hoje.month, 1) + dateutils.relativedelta(months=4) - datetime.timedelta(days=1)
fim_m3 = datetime.datetime(fim_m3.year, fim_m3.month, fim_m3.day, 12, 0, 0)

# preencher completamente as 6 semanas do prevs de dez/2024
df = datetime.datetime(2025, 1, 10, 12, 0, 0)

# MONTAGEM --------------------------------------------------------------
nome_do_estudo='EXEMPLO-MULTICENARIOS'
estudo = BodyFluxPersonalizado()
estudo.set_nome_estudo(nome_do_estudo)
estudo.set_periodo_analise(d1, df)


# SMAP HISTORICO - CHUVA HISTORICA DESDE PRIMEIRO DIA
for anoh in range(2000, hoje.year):
    cen = CenarioFluxPersonalizado()
    cen.set_nome_cenario("SMAPH-{}".format(str(anoh)))

    dhi = datetime.date(anoh, d1.month, d1.day)
    cen.add_bloco(Modelos.MERGE, dhi, True, d1, df)
    estudo.add_cenario(cen)
#end for

# # EXEMPLO UTILIZANDO CHUVA HISTORICA A PARTIR DE UMA DATA ESPECIFICA
# for anoh in range(2000, hoje.year):
#     cen = CenarioFluxPersonalizado()
#     cen.set_nome_cenario("ONS-EC46-H{}".format(str(anoh)))
#     cen.add_bloco(Modelos.NPREVC, hoje, True, d1, d14)
#     cen.add_bloco(Modelos.ECMWF46, dprev_ec46, True, d15, d46)
#
#     dhi = datetime.date(anoh, d47.month, d47.day)
#     cen.add_bloco(Modelos.MERGE, dhi, True, d47, df)
#     estudo.add_cenario(cen)
# #end for

# #EXEMPLO UTILIZANDO MEMBROS DO MODELO ECMWF/ECMWF46
# for m in range(0,51):
#     cen = CenarioFluxPersonalizado()
#     cen.set_nome_cenario("EC15-EC46-MEMBRO{}".format(str(m)))
#     cen.add_bloco(Modelos.ECMWF, hoje, True, d1, d14, runtime=0, membro=m)
#     cen.add_bloco(Modelos.ECMWF46, dprev_ec46, True, d15, d46, runtime=0, membro=m)
#
#     dhi = datetime.date(anoh, d47.month, d47.day)
#     cen.add_bloco(Modelos.MERGE, dhi, True, d47, df)
#     estudo.add_cenario(cen)
# #end for

# # EXEMPLO UTILIZANDO MEMBROS DO MODELO ECMWF46-PROB
# membros = ['p10', 'p25', 'p40', 'p50', 'p60', 'p70', 'p75', 'p80', 'p90']
# for m in membros:
#     cen = CenarioFluxPersonalizado()
#     cen.set_nome_cenario("EC46PROB-{}-CLIMATOLOGIA".format(m))
#     cen.add_bloco(Modelos.ECMWF46, dprev_ec46, True, d1, d46, runtime=0, membro=m)
#     cen.add_bloco(Modelos.CLIMATOLOGIA, hoje, True, d47, df)
#     estudo.add_cenario(cen)
# #end for

# OPÇÕES DO ESTUDO ----------------------------------------------------------
# opções para arquivos de saída
estudo.set_option_produzir_mapas(False)
estudo.set_option_produzir_arquivo_pmed_xlsx(True)
estudo.set_option_produzir_arquivos_dessem(False)
estudo.set_option_produzir_ena_diaria(True)
estudo.set_option_tabelar_prevs_produzidos(True)
estudo.set_option_produzir_prevs_mensais(True)
estudo.set_option_produzir_vazpast(True)

# opções de uso do modelo previvaz
# estudo.set_option_previvaz_produzir_todas_revisoes_intermediarias(True)
# estudo.set_option_previvaz_produzir_somente_rv0_intermediarias(True)
estudo.set_option_previvaz_produzir_somente_revisoes_rvf(True)

# opções de produção de vazões probabilísticas
estudo.set_option_produzir_vmed(True)
estudo.set_option_produzir_vpercentil(True)
estudo.set_option_cenarios_vpercentil([17, 25, 34, 50, 67, 75, 84])


# # opções de produção de vazões por agrupamento
# estudo.set_option_produzir_cenarios_por_agrupamento(True)
# estudo.set_option_numero_agrupamentos(3)
# estudo.set_option_dimensoes_para_agrupamento(DimensoesAgrupamento.SE_S_vs_NE_N.value)

# Definição das opções de produção de vazões ponderadas
estudo.set_option_produzir_vmedponderada(False)
# Lembrar de definir os pesos para ponderação igual ao número de cenários
estudo.set_option_pesos_vmed_ponderada([])

# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"


# EXECUÇÃO --------------------------------------------------------------
flux = FluxPersonalizado(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)
rq = flux.create_request(estudo)
resultado = flux.execute(rq)


# VERIFICAÇÃO DO REQUEST ------------------------------------------------
msg = """
Estudo {NOME} enviado para simulação.
Numeração: {ID}.
Código de identificação única UUID: {UUID}
"""
msg = msg.format(NOME=resultado['ds_nome_estudo'], ID=resultado['id'], UUID=resultado['uuid'])
print(msg)

