from ee_ampere_consultoria.produtos.meteorologia import Meteorologia
from ee_ampere_consultoria.produtos.meteorologia import ModelosDataStore
from ee_ampere_consultoria.produtos.meteorologia import NiveisAtm
from ee_ampere_consultoria.produtos.meteorologia import VariaveisMeteorologicas
from ee_ampere_consultoria.produtos.meteorologia import Runtime
from ee_ampere_consultoria.produtos.meteorologia import  RemocaoVies
from ee_ampere_consultoria.produtos.meteorologia import Membro
import datetime
import hashlib


# DATAS -----------------------------------------------------------------
hoje = datetime.datetime.utcnow() - datetime.timedelta(hours=3)
ontem = hoje - datetime.timedelta(days=1)
d1 = hoje + datetime.timedelta(days=1)
d5 = hoje + datetime.timedelta(days=5)
df = hoje + datetime.timedelta(days=15)


# CREDENCIAIS -----------------------------------------------------------
USERNAME = 'username_exemplo'
senha='senha_exemplo'
MD5_PASSWORD_HASH = hashlib.md5(senha.encode('utf-8')).hexdigest()
USER_ACCESS_TOKEN= "USER-ACCESS-TOKEN-EXEMPLO"


# EXEMPLO 1) ARQUIVOS TXT COMPLETOS DE CHUVA OBSERVADA (MERGE)
# RECOMENDA-SE SEMPRE INCLUIR data_inicial E data_final NESTA REQUISIÇÃO
# CASO CONTRÁRIO, A LISTAGEM RESGATARÁ O HISTÓRICO COMPLETO DESDE 01/01/2000
# COM APROXIMADAMENTE 3,5Gb DE ARQUIVOS TXT e 1 Gb DE IMAGENS PNG
met = Meteorologia(USERNAME,MD5_PASSWORD_HASH,USER_ACCESS_TOKEN)
r = met.data_store_download(ModelosDataStore.MERGE, datetime.date(2023,2,10), NiveisAtm.SingleLevel, VariaveisMeteorologicas.PREC,
                            Runtime.UTC00, RemocaoVies.TRUE, Membro.ENSEMBLE,
                            data_inicial=datetime.date(2023,2,1), data_final=datetime.date(2023,2,10))
f = open("resultados_meteorologia_datastore_merge_exemplo.zip", "wb")
f.write(r)
f.close()



# EXEMPLO 2) TODOS OS ARQUIVOS TXT COMPLETOS DA PREVISÃO ATUAL DO MODELO ECMWFENS
r = met.data_store_download(ModelosDataStore.ECMWFENS, hoje, NiveisAtm.SingleLevel, VariaveisMeteorologicas.PREC,
                            Runtime.UTC00, RemocaoVies.TRUE, Membro.ENSEMBLE)
f = open("resultados_meteorologia_datastore_ecmwfens_txt_exemplo.zip", "wb")
f.write(r)
f.close()



# EXEMPLO 3) TODOS OS MAPAS DA PREVISÃO ATUAL DO MODELO ECMWFENS
r = met.data_store_download(ModelosDataStore.ECMWFENS, hoje, NiveisAtm.SingleLevel, VariaveisMeteorologicas.PREC,
                            Runtime.UTC00, RemocaoVies.TRUE, Membro.ENSEMBLE, png=True)
f = open("resultados_meteorologia_datastore_ecmwfens_png_exemplo.zip", "wb")
f.write(r)
f.close()



# EXEMPLO 4) SOMENTE ALGUNS OS MAPAS DA PREVISÃO ATUAL DO MODELO ECMWFENS
r = met.data_store_download(ModelosDataStore.ECMWFENS, hoje, NiveisAtm.SingleLevel, VariaveisMeteorologicas.PREC,
                            Runtime.UTC00, RemocaoVies.TRUE, Membro.ENSEMBLE,
                            data_inicial=d1, data_final=d5, png=True)
f = open("resultados_meteorologia_datastore_ecmwfens_png2_exemplo.zip", "wb")
f.write(r)
f.close()


# EXEMPLO 5) TODOS MAPAS DA PREVISÃO ATUAL DO MODELO ECMWF46
r = met.data_store_download(ModelosDataStore.ECMWF46, hoje, NiveisAtm.SingleLevel, VariaveisMeteorologicas.PREC,
                            Runtime.UTC00, RemocaoVies.TRUE, Membro.ENSEMBLE, png=True)
f = open("resultados_meteorologia_datastore_ecmwfens_png2_exemplo.zip", "wb")
f.write(r)
f.close()